'use strict';

// Videos controller
angular.module('videos').controller('VideosController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Videos',
	function($scope,$http, $stateParams, $location, Authentication, Videos) {
		$scope.authentication = Authentication;

		$scope.uploadFile = function() {
            $scope.uploadButton = true;
            var fd = new FormData();
            var file = $scope.myFile;
						console.log(file);
            fd.append('file', file);
						console.log(file);
						console.log(fd);
						$http.post('uploads?delay=yes', fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function(d) {
                $scope.filepath = d.msg;
                var url = d.msg;
                url = url.replace(/\\/g, '/');
                url = url.replace('public/', '');
								$scope.url = url;
                // $scope.attach.push({
                //     filepath: url,
                //     descriptionFile: $scope.descriptionFile
                // });
                // $scope.descriptionFile = undefined;
                $scope.uploadButton = true;
                console.log($scope.attach);
                // toastr.success('File Upload ','Succes!');
            }).error(function(){
                // toastr.error('File Upload','Error try Await!');
            });
        };


			$scope.checkUpload = function(){
				return $scope.checkUploadVal === true;
			};

		// Create new Video
		$scope.create = function() {

			$scope.uploadFile();
			console.log('la Url es: ' + this.url);
			// Create new Video object
			var video = new Videos ({
				name: this.name,
				url: this.url,
				language: this.language,
			});

			// Redirect after save
			video.$save(function(response) {
				$location.path('videos/' + response._id);

				// Clear form fields
				$scope.name = '';
				$scope.url = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Video
		$scope.remove = function(video) {
			if ( video ) {
				video.$remove();

				for (var i in $scope.videos) {
					if ($scope.videos [i] === video) {
						$scope.videos.splice(i, 1);
					}
				}
			} else {
				$scope.video.$remove(function() {
					$location.path('videos');
				});
			}
		};

		// Update existing Video
		$scope.update = function() {
			var video = $scope.video;

			video.$update(function() {
				$location.path('videos/' + video._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Videos
		$scope.find = function() {
			$scope.videos = Videos.query();
		};

		// Find existing Video
		$scope.findOne = function() {
			$scope.video = Videos.get({
				videoId: $stateParams.videoId
			});
		};
	}
]);
