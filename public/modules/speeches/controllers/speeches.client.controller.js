'use strict';

// Speeches controller
angular.module('speeches').controller('SpeechesController', ['$scope', '$http','$stateParams', '$location', 'Authentication', 'Speeches','multiStepForm','QuestionCaptions',
	function($scope, $http ,$stateParams, $location, Authentication, Speeches,  multiStepForm,QuestionCaptions) {
		$scope.authentication = Authentication;
		$scope.stepAnswer = 1;
		$scope.nextStep = function() {
					 $scope.stepAnswer++;
		};
		$scope.prevStep = function() {
					 $scope.stepAnswer--;
		}

		// $scope.$nextQuestion = function() {
		// 	alert();
		// }
		// $scope.$watch('speechTextResp', function(val) {
		// 	console.log(val);
		// 	// console.log(angular.copy(multiStepFormScope.model));
		// //  console.log($scope.lastName);
		// });
		$scope.changeText=function(){
			// alert();
		};

		$scope.contadorQuestions = 0;

		$scope.stepsChange = function(pasoActual){

				if (pasoActual != $scope.contadorQuestions) {
						return false;
				}else {
					return true;
				}

		};

		$scope.timeControl = function(start, end){
			jwplayer().seek(start).onTime(function (event) {
					if(event.position >= end ) {
							this.stop();
					}
			});
		}

		$scope.EmpezarTest = function(){
				var tiempoInicio = $scope.answerData[0].start_second;
				var tiempoFinal = $scope.answerData[0].end_second;
				// console.log(tiempoInicio);
				// console.log(tiempoFinal);
				$scope.timeControl(tiempoInicio,tiempoFinal);
		}

		$scope.siguienteStep = function(){

			var limite = $scope.questionCaptions.length - 2
			// console.log(limite);
			// console.log($scope.contadorQuestions);

			if ($scope.contadorQuestions < limite) {
						$scope.contadorQuestions ++;
			}

			var tiempoInicio = $scope.answerData[$scope.contadorQuestions].start_second;
			var tiempoFinal = $scope.answerData[$scope.contadorQuestions].end_second;
			// console.log(tiempoInicio);
			// console.log(tiempoFinal);
			$scope.timeControl(tiempoInicio,tiempoFinal);

			return;
		}

		$scope.anteriorStep = function(){

			if ($scope.contadorQuestions != 0) {

					$scope.contadorQuestions --;

				var conta = $scope.contadorQuestions;
				var tiempoInicio = $scope.answerData[conta].start_second;
				var tiempoFinal = $scope.answerData[conta].end_second;
				// console.log(tiempoInicio);
				// console.log(tiempoFinal);
				$scope.timeControl(tiempoInicio,tiempoFinal);

			}



		}

		$scope.init = function(){
			// console.log('tamanio: ' + $scope.questionCaptions.length);
			$scope.questionCaptions = QuestionCaptions.query();
			$http.get('asnwerByVideoId' , {
            params: {
							i:'57bb5f04d91835339a97cc55'
            }
          }).then(function(response){
						// cosnole.log(response.data);
            return response.data;
          });

		};

		// Create new Speech
		$scope.create = function() {
			// Create new Speech object
			var speech = new Speeches ({
				name: this.name
			});

			// Redirect after save
			speech.$save(function(response) {
				$location.path('speeches/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Speech
		$scope.remove = function(speech) {
			if ( speech ) {
				speech.$remove();

				for (var i in $scope.speeches) {
					if ($scope.speeches [i] === speech) {
						$scope.speeches.splice(i, 1);
					}
				}
			} else {
				$scope.speech.$remove(function() {
					$location.path('speeches');
				});
			}
		};

		// Update existing Speech
		$scope.update = function() {
			var speech = $scope.speech;

			speech.$update(function() {
				$location.path('speeches/' + speech._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Speeches
		$scope.find = function() {
			$scope.speeches = Speeches.query();
		};




		$scope.buscarRespuesta = function(p_idQuestion){
			var porcentajeRespuesta;
			$scope.replyDataByUser.forEach(function(item) {
						if (item.id_question == p_idQuestion) {
								porcentajeRespuesta = item.replyPorcent;
						}
			});

				return porcentajeRespuesta;
		}
		// Find existing Speech
		$scope.findOne = function() {
			$scope.speech = Speeches.get({
				speechId: $stateParams.speechId
			}).$promise.then(function(result) {
				$scope.questionCaptions = QuestionCaptions.query();
				// console.log($scope.speech);
				$http.get('asnwerByVideoId' , {
							params: {
								i:result.id_video
							}
						}).then(function(response){
							// aqui esta la data..
							// console.log(response.data);
							$scope.answerData = response.data;
							// console.log($scope.answerData.length);
						});
			});
			$http.get('reply-answersByUser').then(function(response){
						console.log(response.data);
						// return response.data;
							$scope.replyDataByUser = response.data;
					});

		};

	}
]);
