'use strict';

// Configuring the Articles module
angular.module('speeches').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Speeches', 'speeches');
		// Menus.addSubMenuItem('topbar', 'speeches', 'List Speeches', 'speeches');
		// Menus.addSubMenuItem('topbar', 'speeches', 'New Speech', 'speeches/create');
	}
]);
