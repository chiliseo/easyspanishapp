'use strict';

//Setting up route
angular.module('speeches').config(['$stateProvider',
	function($stateProvider) {
		// Speeches state routing
		$stateProvider.
		state('listSpeeches', {
			url: '/speeches',
			templateUrl: 'modules/speeches/views/list-speeches.client.view.html'
		}).
		state('createSpeech', {
			url: '/speeches/create',
			templateUrl: 'modules/speeches/views/create-speech.client.view.html'
		}).
		state('viewSpeech', {
			url: '/speeches/:speechId',
			templateUrl: 'modules/speeches/views/view-speech.client.view.html'
		}).
		state('editSpeech', {
			url: '/speeches/:speechId/edit',
			templateUrl: 'modules/speeches/views/edit-speech.client.view.html'
		});
	}
]);