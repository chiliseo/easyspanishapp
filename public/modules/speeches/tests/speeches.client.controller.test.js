'use strict';

(function() {
	// Speeches Controller Spec
	describe('Speeches Controller Tests', function() {
		// Initialize global variables
		var SpeechesController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Speeches controller.
			SpeechesController = $controller('SpeechesController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Speech object fetched from XHR', inject(function(Speeches) {
			// Create sample Speech using the Speeches service
			var sampleSpeech = new Speeches({
				name: 'New Speech'
			});

			// Create a sample Speeches array that includes the new Speech
			var sampleSpeeches = [sampleSpeech];

			// Set GET response
			$httpBackend.expectGET('speeches').respond(sampleSpeeches);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.speeches).toEqualData(sampleSpeeches);
		}));

		it('$scope.findOne() should create an array with one Speech object fetched from XHR using a speechId URL parameter', inject(function(Speeches) {
			// Define a sample Speech object
			var sampleSpeech = new Speeches({
				name: 'New Speech'
			});

			// Set the URL parameter
			$stateParams.speechId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/speeches\/([0-9a-fA-F]{24})$/).respond(sampleSpeech);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.speech).toEqualData(sampleSpeech);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Speeches) {
			// Create a sample Speech object
			var sampleSpeechPostData = new Speeches({
				name: 'New Speech'
			});

			// Create a sample Speech response
			var sampleSpeechResponse = new Speeches({
				_id: '525cf20451979dea2c000001',
				name: 'New Speech'
			});

			// Fixture mock form input values
			scope.name = 'New Speech';

			// Set POST response
			$httpBackend.expectPOST('speeches', sampleSpeechPostData).respond(sampleSpeechResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Speech was created
			expect($location.path()).toBe('/speeches/' + sampleSpeechResponse._id);
		}));

		it('$scope.update() should update a valid Speech', inject(function(Speeches) {
			// Define a sample Speech put data
			var sampleSpeechPutData = new Speeches({
				_id: '525cf20451979dea2c000001',
				name: 'New Speech'
			});

			// Mock Speech in scope
			scope.speech = sampleSpeechPutData;

			// Set PUT response
			$httpBackend.expectPUT(/speeches\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/speeches/' + sampleSpeechPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid speechId and remove the Speech from the scope', inject(function(Speeches) {
			// Create new Speech object
			var sampleSpeech = new Speeches({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Speeches array and include the Speech
			scope.speeches = [sampleSpeech];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/speeches\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleSpeech);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.speeches.length).toBe(0);
		}));
	});
}());