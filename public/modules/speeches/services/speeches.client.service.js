'use strict';

//Speeches service used to communicate Speeches REST endpoints
angular.module('speeches').factory('Speeches', ['$resource',
	function($resource) {
		return $resource('speeches/:speechId', { speechId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);