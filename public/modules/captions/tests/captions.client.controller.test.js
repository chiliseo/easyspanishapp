'use strict';

(function() {
	// Captions Controller Spec
	describe('Captions Controller Tests', function() {
		// Initialize global variables
		var CaptionsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Captions controller.
			CaptionsController = $controller('CaptionsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Caption object fetched from XHR', inject(function(Captions) {
			// Create sample Caption using the Captions service
			var sampleCaption = new Captions({
				name: 'New Caption'
			});

			// Create a sample Captions array that includes the new Caption
			var sampleCaptions = [sampleCaption];

			// Set GET response
			$httpBackend.expectGET('captions').respond(sampleCaptions);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.captions).toEqualData(sampleCaptions);
		}));

		it('$scope.findOne() should create an array with one Caption object fetched from XHR using a captionId URL parameter', inject(function(Captions) {
			// Define a sample Caption object
			var sampleCaption = new Captions({
				name: 'New Caption'
			});

			// Set the URL parameter
			$stateParams.captionId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/captions\/([0-9a-fA-F]{24})$/).respond(sampleCaption);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.caption).toEqualData(sampleCaption);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Captions) {
			// Create a sample Caption object
			var sampleCaptionPostData = new Captions({
				name: 'New Caption'
			});

			// Create a sample Caption response
			var sampleCaptionResponse = new Captions({
				_id: '525cf20451979dea2c000001',
				name: 'New Caption'
			});

			// Fixture mock form input values
			scope.name = 'New Caption';

			// Set POST response
			$httpBackend.expectPOST('captions', sampleCaptionPostData).respond(sampleCaptionResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Caption was created
			expect($location.path()).toBe('/captions/' + sampleCaptionResponse._id);
		}));

		it('$scope.update() should update a valid Caption', inject(function(Captions) {
			// Define a sample Caption put data
			var sampleCaptionPutData = new Captions({
				_id: '525cf20451979dea2c000001',
				name: 'New Caption'
			});

			// Mock Caption in scope
			scope.caption = sampleCaptionPutData;

			// Set PUT response
			$httpBackend.expectPUT(/captions\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/captions/' + sampleCaptionPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid captionId and remove the Caption from the scope', inject(function(Captions) {
			// Create new Caption object
			var sampleCaption = new Captions({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Captions array and include the Caption
			scope.captions = [sampleCaption];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/captions\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleCaption);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.captions.length).toBe(0);
		}));
	});
}());