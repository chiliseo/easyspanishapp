'use strict';

//Setting up route
angular.module('captions').config(['$stateProvider',
	function($stateProvider) {
		// Captions state routing
		$stateProvider.
		state('listCaptions', {
			url: '/captions',
			templateUrl: 'modules/captions/views/list-captions.client.view.html'
		}).
		state('createCaption', {
			url: '/captions/create',
			templateUrl: 'modules/captions/views/create-caption.client.view.html'
		}).
		state('viewCaption', {
			url: '/captions/:captionId',
			templateUrl: 'modules/captions/views/view-caption.client.view.html'
		}).
		state('editCaption', {
			url: '/captions/:captionId/edit',
			templateUrl: 'modules/captions/views/edit-caption.client.view.html'
		});
	}
]);