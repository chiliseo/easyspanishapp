'use strict';

// Configuring the Articles module
angular.module('captions').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Captions', 'captions', 'dropdown', '/captions(/create)?');
		Menus.addSubMenuItem('topbar', 'captions', 'List Captions', 'captions');
		Menus.addSubMenuItem('topbar', 'captions', 'New Caption', 'captions/create');
	}
]);