'use strict';

//Captions service used to communicate Captions REST endpoints
angular.module('captions').factory('Captions', ['$resource',
	function($resource) {
		return $resource('captions/:captionId', { captionId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);