'use strict';

// Captions controller
angular.module('captions').controller('CaptionsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Captions','Videos',
	function($scope, $stateParams, $location, Authentication, Captions, Videos) {
		$scope.authentication = Authentication;

		$scope.init = function() {
			$scope.captions = Captions.query();
			$scope.videos = Videos.query();
		};

		// Create new Caption
		$scope.create = function() {

			console.log(this.videos)
			// Create new Caption object
			var caption = new Captions ({
				name: this.name,
				id_video: this.idVideo,
				url: this.url,
			});

			// Redirect after save
			caption.$save(function(response) {
				$location.path('captions/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Caption
		$scope.remove = function(caption) {
			if ( caption ) {
				caption.$remove();

				for (var i in $scope.captions) {
					if ($scope.captions [i] === caption) {
						$scope.captions.splice(i, 1);
					}
				}
			} else {
				$scope.caption.$remove(function() {
					$location.path('captions');
				});
			}
		};

		// Update existing Caption
		$scope.update = function() {
			var caption = $scope.caption;

			caption.$update(function() {
				$location.path('captions/' + caption._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Captions
		$scope.find = function() {
			$scope.captions = Captions.query();
			$scope.videos = Videos.query();
		};

		// Find existing Caption
		$scope.findOne = function() {
			$scope.caption = Captions.get({
				captionId: $stateParams.captionId
			});
		};
	}
]);
