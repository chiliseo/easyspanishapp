'use strict';

//Setting up route
angular.module('reply-answers').config(['$stateProvider',
	function($stateProvider) {
		// Reply answers state routing
		$stateProvider.
		state('listReplyAnswers', {
			url: '/reply-answers',
			templateUrl: 'modules/reply-answers/views/list-reply-answers.client.view.html'
		}).
		state('createReplyAnswer', {
			url: '/reply-answers/create',
			templateUrl: 'modules/reply-answers/views/create-reply-answer.client.view.html'
		}).
		state('viewReplyAnswer', {
			url: '/reply-answers/:replyAnswerId',
			templateUrl: 'modules/reply-answers/views/view-reply-answer.client.view.html'
		}).
		state('editReplyAnswer', {
			url: '/reply-answers/:replyAnswerId/edit',
			templateUrl: 'modules/reply-answers/views/edit-reply-answer.client.view.html'
		});
	}
]);