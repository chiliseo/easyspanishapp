'use strict';

// Reply answers controller
angular.module('reply-answers').controller('ReplyAnswersController', ['$scope', '$stateParams', '$location', 'Authentication', 'ReplyAnswers','QuestionCaptions',
	function($scope, $stateParams, $location, Authentication, ReplyAnswers,QuestionCaptions) {
		$scope.authentication = Authentication;

		$scope.init=function(){
			$scope.questionCaptions = QuestionCaptions.query();
		};
		// Create new Reply answer
		$scope.create = function() {
			// Create new Reply answer object
			var replyAnswer = new ReplyAnswers ({
				id_question:this.question,
				replyPorcent: this.replyPorcent
			});

			// Redirect after save
			replyAnswer.$save(function(response) {
				$location.path('reply-answers/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Reply answer
		$scope.remove = function(replyAnswer) {
			if ( replyAnswer ) {
				replyAnswer.$remove();

				for (var i in $scope.replyAnswers) {
					if ($scope.replyAnswers [i] === replyAnswer) {
						$scope.replyAnswers.splice(i, 1);
					}
				}
			} else {
				$scope.replyAnswer.$remove(function() {
					$location.path('reply-answers');
				});
			}
		};

		// Update existing Reply answer
		$scope.update = function() {
			var replyAnswer = $scope.replyAnswer;

			replyAnswer.$update(function() {
				$location.path('reply-answers/' + replyAnswer._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Reply answers
		$scope.find = function() {
			$scope.replyAnswers = ReplyAnswers.query();
		};

		// Find existing Reply answer
		$scope.findOne = function() {
			$scope.replyAnswer = ReplyAnswers.get({
				replyAnswerId: $stateParams.replyAnswerId
			});
		};
	}
]);
