'use strict';

(function() {
	// Reply answers Controller Spec
	describe('Reply answers Controller Tests', function() {
		// Initialize global variables
		var ReplyAnswersController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Reply answers controller.
			ReplyAnswersController = $controller('ReplyAnswersController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Reply answer object fetched from XHR', inject(function(ReplyAnswers) {
			// Create sample Reply answer using the Reply answers service
			var sampleReplyAnswer = new ReplyAnswers({
				name: 'New Reply answer'
			});

			// Create a sample Reply answers array that includes the new Reply answer
			var sampleReplyAnswers = [sampleReplyAnswer];

			// Set GET response
			$httpBackend.expectGET('reply-answers').respond(sampleReplyAnswers);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.replyAnswers).toEqualData(sampleReplyAnswers);
		}));

		it('$scope.findOne() should create an array with one Reply answer object fetched from XHR using a replyAnswerId URL parameter', inject(function(ReplyAnswers) {
			// Define a sample Reply answer object
			var sampleReplyAnswer = new ReplyAnswers({
				name: 'New Reply answer'
			});

			// Set the URL parameter
			$stateParams.replyAnswerId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/reply-answers\/([0-9a-fA-F]{24})$/).respond(sampleReplyAnswer);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.replyAnswer).toEqualData(sampleReplyAnswer);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(ReplyAnswers) {
			// Create a sample Reply answer object
			var sampleReplyAnswerPostData = new ReplyAnswers({
				name: 'New Reply answer'
			});

			// Create a sample Reply answer response
			var sampleReplyAnswerResponse = new ReplyAnswers({
				_id: '525cf20451979dea2c000001',
				name: 'New Reply answer'
			});

			// Fixture mock form input values
			scope.name = 'New Reply answer';

			// Set POST response
			$httpBackend.expectPOST('reply-answers', sampleReplyAnswerPostData).respond(sampleReplyAnswerResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Reply answer was created
			expect($location.path()).toBe('/reply-answers/' + sampleReplyAnswerResponse._id);
		}));

		it('$scope.update() should update a valid Reply answer', inject(function(ReplyAnswers) {
			// Define a sample Reply answer put data
			var sampleReplyAnswerPutData = new ReplyAnswers({
				_id: '525cf20451979dea2c000001',
				name: 'New Reply answer'
			});

			// Mock Reply answer in scope
			scope.replyAnswer = sampleReplyAnswerPutData;

			// Set PUT response
			$httpBackend.expectPUT(/reply-answers\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/reply-answers/' + sampleReplyAnswerPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid replyAnswerId and remove the Reply answer from the scope', inject(function(ReplyAnswers) {
			// Create new Reply answer object
			var sampleReplyAnswer = new ReplyAnswers({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Reply answers array and include the Reply answer
			scope.replyAnswers = [sampleReplyAnswer];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/reply-answers\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleReplyAnswer);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.replyAnswers.length).toBe(0);
		}));
	});
}());