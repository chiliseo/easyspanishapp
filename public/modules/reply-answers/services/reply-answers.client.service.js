'use strict';

//Reply answers service used to communicate Reply answers REST endpoints
angular.module('reply-answers').factory('ReplyAnswers', ['$resource',
	function($resource) {
		return $resource('reply-answers/:replyAnswerId', { replyAnswerId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);