'use strict';

angular.module('articles').controller('ArticlesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Articles','multiStepForm',
	function($scope, $stateParams, $location, Authentication, Articles, multiStepForm) {
		$scope.authentication = Authentication;

		$scope.steps = [
    // {template: 'Hello <button class="btn btn-default" ng-click="$nextStep()">Next</button>'},
    // {template: 'World <button class="btn btn-default" ng-click="$previousStep()">Previous</button>'}
		{template:'<span>Sometimes oppressing others try one.</span><div class="controls"><label class="control-label" for="title">Respuesta: </label> <input type="text" name="name" value=""></div>'},
		{template:'<span>You ___ from Germany</span><div class="controls"><label class="control-label" for="title">Respuesta: </label> <input type="text" name="name" value=""></div>'}
		];

		$scope.create = function() {
			var article = new Articles({
				title: this.title,
				content: this.content
			});
			article.$save(function(response) {
				$location.path('articles/' + response._id);

				$scope.title = '';
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};
		$scope.goNext = function(i){

			$('[href=#step'+(i+1)+']').tab('show');
			return false;

		}
	}
]);
