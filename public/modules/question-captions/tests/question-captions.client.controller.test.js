'use strict';

(function() {
	// Question captions Controller Spec
	describe('Question captions Controller Tests', function() {
		// Initialize global variables
		var QuestionCaptionsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Question captions controller.
			QuestionCaptionsController = $controller('QuestionCaptionsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Question caption object fetched from XHR', inject(function(QuestionCaptions) {
			// Create sample Question caption using the Question captions service
			var sampleQuestionCaption = new QuestionCaptions({
				name: 'New Question caption'
			});

			// Create a sample Question captions array that includes the new Question caption
			var sampleQuestionCaptions = [sampleQuestionCaption];

			// Set GET response
			$httpBackend.expectGET('question-captions').respond(sampleQuestionCaptions);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.questionCaptions).toEqualData(sampleQuestionCaptions);
		}));

		it('$scope.findOne() should create an array with one Question caption object fetched from XHR using a questionCaptionId URL parameter', inject(function(QuestionCaptions) {
			// Define a sample Question caption object
			var sampleQuestionCaption = new QuestionCaptions({
				name: 'New Question caption'
			});

			// Set the URL parameter
			$stateParams.questionCaptionId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/question-captions\/([0-9a-fA-F]{24})$/).respond(sampleQuestionCaption);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.questionCaption).toEqualData(sampleQuestionCaption);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(QuestionCaptions) {
			// Create a sample Question caption object
			var sampleQuestionCaptionPostData = new QuestionCaptions({
				name: 'New Question caption'
			});

			// Create a sample Question caption response
			var sampleQuestionCaptionResponse = new QuestionCaptions({
				_id: '525cf20451979dea2c000001',
				name: 'New Question caption'
			});

			// Fixture mock form input values
			scope.name = 'New Question caption';

			// Set POST response
			$httpBackend.expectPOST('question-captions', sampleQuestionCaptionPostData).respond(sampleQuestionCaptionResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Question caption was created
			expect($location.path()).toBe('/question-captions/' + sampleQuestionCaptionResponse._id);
		}));

		it('$scope.update() should update a valid Question caption', inject(function(QuestionCaptions) {
			// Define a sample Question caption put data
			var sampleQuestionCaptionPutData = new QuestionCaptions({
				_id: '525cf20451979dea2c000001',
				name: 'New Question caption'
			});

			// Mock Question caption in scope
			scope.questionCaption = sampleQuestionCaptionPutData;

			// Set PUT response
			$httpBackend.expectPUT(/question-captions\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/question-captions/' + sampleQuestionCaptionPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid questionCaptionId and remove the Question caption from the scope', inject(function(QuestionCaptions) {
			// Create new Question caption object
			var sampleQuestionCaption = new QuestionCaptions({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Question captions array and include the Question caption
			scope.questionCaptions = [sampleQuestionCaption];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/question-captions\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleQuestionCaption);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.questionCaptions.length).toBe(0);
		}));
	});
}());