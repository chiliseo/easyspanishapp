'use strict';

// Configuring the Articles module
angular.module('question-captions').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Question captions', 'question-captions', 'dropdown', '/question-captions(/create)?');
		Menus.addSubMenuItem('topbar', 'question-captions', 'List Question captions', 'question-captions');
		Menus.addSubMenuItem('topbar', 'question-captions', 'New Question caption', 'question-captions/create');
	}
]);