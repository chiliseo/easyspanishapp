'use strict';

//Setting up route
angular.module('question-captions').config(['$stateProvider',
	function($stateProvider) {
		// Question captions state routing
		$stateProvider.
		state('listQuestionCaptions', {
			url: '/question-captions',
			templateUrl: 'modules/question-captions/views/list-question-captions.client.view.html'
		}).
		state('createQuestionCaption', {
			url: '/question-captions/create',
			templateUrl: 'modules/question-captions/views/create-question-caption.client.view.html'
		}).
		state('viewQuestionCaption', {
			url: '/question-captions/:questionCaptionId',
			templateUrl: 'modules/question-captions/views/view-question-caption.client.view.html'
		}).
		state('editQuestionCaption', {
			url: '/question-captions/:questionCaptionId/edit',
			templateUrl: 'modules/question-captions/views/edit-question-caption.client.view.html'
		});
	}
]);