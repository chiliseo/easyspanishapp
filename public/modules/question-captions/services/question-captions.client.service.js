'use strict';

//Question captions service used to communicate Question captions REST endpoints
angular.module('question-captions').factory('QuestionCaptions', ['$resource',
	function($resource) {
		return $resource('question-captions/:questionCaptionId', { questionCaptionId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);