'use strict';

// Question captions controller
angular.module('question-captions').controller('QuestionCaptionsController', ['$scope', '$stateParams', '$location', 'Authentication', 'QuestionCaptions', 'Videos',
	function($scope, $stateParams, $location, Authentication, QuestionCaptions, Videos) {
		$scope.authentication = Authentication;


		$scope.init = function() {
			$scope.endText = "testing";
			$scope.startText = "testing";
			$scope.videos = Videos.query();
		};

		// Create new Question caption
		$scope.create = function() {
			console.log(this);
			// Create new Question caption object
			var questionCaption = new QuestionCaptions ({
				id_video: this.idVideo,
				question_type: this.typeQuestion,
				start_second: this.startSecond,
				end_second: this.endSecond,
				start_text: this.startText,
				answer_text: this.answerText,
				end_text: this.endText,
			});

			// Redirect after save
			questionCaption.$save(function(response) {
				$location.path('question-captions/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Question caption
		$scope.remove = function(questionCaption) {
			if ( questionCaption ) {
				questionCaption.$remove();

				for (var i in $scope.questionCaptions) {
					if ($scope.questionCaptions [i] === questionCaption) {
						$scope.questionCaptions.splice(i, 1);
					}
				}
			} else {
				$scope.questionCaption.$remove(function() {
					$location.path('question-captions');
				});
			}
		};

		// Update existing Question caption
		$scope.update = function() {
			var questionCaption = $scope.questionCaption;

			questionCaption.$update(function() {
				$location.path('question-captions/' + questionCaption._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Question captions
		$scope.find = function() {
			$scope.questionCaptions = QuestionCaptions.query();
		};

		// Find existing Question caption
		$scope.findOne = function() {
			$scope.questionCaption = QuestionCaptions.get({
				questionCaptionId: $stateParams.questionCaptionId
			});
		};
	}
]);
