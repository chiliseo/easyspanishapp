'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Caption = mongoose.model('Caption'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, caption;

/**
 * Caption routes tests
 */
describe('Caption CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Caption
		user.save(function() {
			caption = {
				name: 'Caption Name'
			};

			done();
		});
	});

	it('should be able to save Caption instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Caption
				agent.post('/captions')
					.send(caption)
					.expect(200)
					.end(function(captionSaveErr, captionSaveRes) {
						// Handle Caption save error
						if (captionSaveErr) done(captionSaveErr);

						// Get a list of Captions
						agent.get('/captions')
							.end(function(captionsGetErr, captionsGetRes) {
								// Handle Caption save error
								if (captionsGetErr) done(captionsGetErr);

								// Get Captions list
								var captions = captionsGetRes.body;

								// Set assertions
								(captions[0].user._id).should.equal(userId);
								(captions[0].name).should.match('Caption Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Caption instance if not logged in', function(done) {
		agent.post('/captions')
			.send(caption)
			.expect(401)
			.end(function(captionSaveErr, captionSaveRes) {
				// Call the assertion callback
				done(captionSaveErr);
			});
	});

	it('should not be able to save Caption instance if no name is provided', function(done) {
		// Invalidate name field
		caption.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Caption
				agent.post('/captions')
					.send(caption)
					.expect(400)
					.end(function(captionSaveErr, captionSaveRes) {
						// Set message assertion
						(captionSaveRes.body.message).should.match('Please fill Caption name');
						
						// Handle Caption save error
						done(captionSaveErr);
					});
			});
	});

	it('should be able to update Caption instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Caption
				agent.post('/captions')
					.send(caption)
					.expect(200)
					.end(function(captionSaveErr, captionSaveRes) {
						// Handle Caption save error
						if (captionSaveErr) done(captionSaveErr);

						// Update Caption name
						caption.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Caption
						agent.put('/captions/' + captionSaveRes.body._id)
							.send(caption)
							.expect(200)
							.end(function(captionUpdateErr, captionUpdateRes) {
								// Handle Caption update error
								if (captionUpdateErr) done(captionUpdateErr);

								// Set assertions
								(captionUpdateRes.body._id).should.equal(captionSaveRes.body._id);
								(captionUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Captions if not signed in', function(done) {
		// Create new Caption model instance
		var captionObj = new Caption(caption);

		// Save the Caption
		captionObj.save(function() {
			// Request Captions
			request(app).get('/captions')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Caption if not signed in', function(done) {
		// Create new Caption model instance
		var captionObj = new Caption(caption);

		// Save the Caption
		captionObj.save(function() {
			request(app).get('/captions/' + captionObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', caption.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Caption instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Caption
				agent.post('/captions')
					.send(caption)
					.expect(200)
					.end(function(captionSaveErr, captionSaveRes) {
						// Handle Caption save error
						if (captionSaveErr) done(captionSaveErr);

						// Delete existing Caption
						agent.delete('/captions/' + captionSaveRes.body._id)
							.send(caption)
							.expect(200)
							.end(function(captionDeleteErr, captionDeleteRes) {
								// Handle Caption error error
								if (captionDeleteErr) done(captionDeleteErr);

								// Set assertions
								(captionDeleteRes.body._id).should.equal(captionSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Caption instance if not signed in', function(done) {
		// Set Caption user 
		caption.user = user;

		// Create new Caption model instance
		var captionObj = new Caption(caption);

		// Save the Caption
		captionObj.save(function() {
			// Try deleting Caption
			request(app).delete('/captions/' + captionObj._id)
			.expect(401)
			.end(function(captionDeleteErr, captionDeleteRes) {
				// Set message assertion
				(captionDeleteRes.body.message).should.match('User is not logged in');

				// Handle Caption error error
				done(captionDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Caption.remove().exec();
		done();
	});
});