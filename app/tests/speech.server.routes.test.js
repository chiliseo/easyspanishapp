'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Speech = mongoose.model('Speech'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, speech;

/**
 * Speech routes tests
 */
describe('Speech CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Speech
		user.save(function() {
			speech = {
				name: 'Speech Name'
			};

			done();
		});
	});

	it('should be able to save Speech instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Speech
				agent.post('/speeches')
					.send(speech)
					.expect(200)
					.end(function(speechSaveErr, speechSaveRes) {
						// Handle Speech save error
						if (speechSaveErr) done(speechSaveErr);

						// Get a list of Speeches
						agent.get('/speeches')
							.end(function(speechesGetErr, speechesGetRes) {
								// Handle Speech save error
								if (speechesGetErr) done(speechesGetErr);

								// Get Speeches list
								var speeches = speechesGetRes.body;

								// Set assertions
								(speeches[0].user._id).should.equal(userId);
								(speeches[0].name).should.match('Speech Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Speech instance if not logged in', function(done) {
		agent.post('/speeches')
			.send(speech)
			.expect(401)
			.end(function(speechSaveErr, speechSaveRes) {
				// Call the assertion callback
				done(speechSaveErr);
			});
	});

	it('should not be able to save Speech instance if no name is provided', function(done) {
		// Invalidate name field
		speech.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Speech
				agent.post('/speeches')
					.send(speech)
					.expect(400)
					.end(function(speechSaveErr, speechSaveRes) {
						// Set message assertion
						(speechSaveRes.body.message).should.match('Please fill Speech name');
						
						// Handle Speech save error
						done(speechSaveErr);
					});
			});
	});

	it('should be able to update Speech instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Speech
				agent.post('/speeches')
					.send(speech)
					.expect(200)
					.end(function(speechSaveErr, speechSaveRes) {
						// Handle Speech save error
						if (speechSaveErr) done(speechSaveErr);

						// Update Speech name
						speech.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Speech
						agent.put('/speeches/' + speechSaveRes.body._id)
							.send(speech)
							.expect(200)
							.end(function(speechUpdateErr, speechUpdateRes) {
								// Handle Speech update error
								if (speechUpdateErr) done(speechUpdateErr);

								// Set assertions
								(speechUpdateRes.body._id).should.equal(speechSaveRes.body._id);
								(speechUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Speeches if not signed in', function(done) {
		// Create new Speech model instance
		var speechObj = new Speech(speech);

		// Save the Speech
		speechObj.save(function() {
			// Request Speeches
			request(app).get('/speeches')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Speech if not signed in', function(done) {
		// Create new Speech model instance
		var speechObj = new Speech(speech);

		// Save the Speech
		speechObj.save(function() {
			request(app).get('/speeches/' + speechObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', speech.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Speech instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Speech
				agent.post('/speeches')
					.send(speech)
					.expect(200)
					.end(function(speechSaveErr, speechSaveRes) {
						// Handle Speech save error
						if (speechSaveErr) done(speechSaveErr);

						// Delete existing Speech
						agent.delete('/speeches/' + speechSaveRes.body._id)
							.send(speech)
							.expect(200)
							.end(function(speechDeleteErr, speechDeleteRes) {
								// Handle Speech error error
								if (speechDeleteErr) done(speechDeleteErr);

								// Set assertions
								(speechDeleteRes.body._id).should.equal(speechSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Speech instance if not signed in', function(done) {
		// Set Speech user 
		speech.user = user;

		// Create new Speech model instance
		var speechObj = new Speech(speech);

		// Save the Speech
		speechObj.save(function() {
			// Try deleting Speech
			request(app).delete('/speeches/' + speechObj._id)
			.expect(401)
			.end(function(speechDeleteErr, speechDeleteRes) {
				// Set message assertion
				(speechDeleteRes.body.message).should.match('User is not logged in');

				// Handle Speech error error
				done(speechDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Speech.remove().exec();
		done();
	});
});