'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	QuestionCaption = mongoose.model('QuestionCaption');

/**
 * Globals
 */
var user, questionCaption;

/**
 * Unit tests
 */
describe('Question caption Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			questionCaption = new QuestionCaption({
				name: 'Question caption Name',
				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return questionCaption.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			questionCaption.name = '';

			return questionCaption.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		QuestionCaption.remove().exec();
		User.remove().exec();

		done();
	});
});