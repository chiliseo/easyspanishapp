'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	ReplyAnswer = mongoose.model('ReplyAnswer'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, replyAnswer;

/**
 * Reply answer routes tests
 */
describe('Reply answer CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Reply answer
		user.save(function() {
			replyAnswer = {
				name: 'Reply answer Name'
			};

			done();
		});
	});

	it('should be able to save Reply answer instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Reply answer
				agent.post('/reply-answers')
					.send(replyAnswer)
					.expect(200)
					.end(function(replyAnswerSaveErr, replyAnswerSaveRes) {
						// Handle Reply answer save error
						if (replyAnswerSaveErr) done(replyAnswerSaveErr);

						// Get a list of Reply answers
						agent.get('/reply-answers')
							.end(function(replyAnswersGetErr, replyAnswersGetRes) {
								// Handle Reply answer save error
								if (replyAnswersGetErr) done(replyAnswersGetErr);

								// Get Reply answers list
								var replyAnswers = replyAnswersGetRes.body;

								// Set assertions
								(replyAnswers[0].user._id).should.equal(userId);
								(replyAnswers[0].name).should.match('Reply answer Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Reply answer instance if not logged in', function(done) {
		agent.post('/reply-answers')
			.send(replyAnswer)
			.expect(401)
			.end(function(replyAnswerSaveErr, replyAnswerSaveRes) {
				// Call the assertion callback
				done(replyAnswerSaveErr);
			});
	});

	it('should not be able to save Reply answer instance if no name is provided', function(done) {
		// Invalidate name field
		replyAnswer.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Reply answer
				agent.post('/reply-answers')
					.send(replyAnswer)
					.expect(400)
					.end(function(replyAnswerSaveErr, replyAnswerSaveRes) {
						// Set message assertion
						(replyAnswerSaveRes.body.message).should.match('Please fill Reply answer name');
						
						// Handle Reply answer save error
						done(replyAnswerSaveErr);
					});
			});
	});

	it('should be able to update Reply answer instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Reply answer
				agent.post('/reply-answers')
					.send(replyAnswer)
					.expect(200)
					.end(function(replyAnswerSaveErr, replyAnswerSaveRes) {
						// Handle Reply answer save error
						if (replyAnswerSaveErr) done(replyAnswerSaveErr);

						// Update Reply answer name
						replyAnswer.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Reply answer
						agent.put('/reply-answers/' + replyAnswerSaveRes.body._id)
							.send(replyAnswer)
							.expect(200)
							.end(function(replyAnswerUpdateErr, replyAnswerUpdateRes) {
								// Handle Reply answer update error
								if (replyAnswerUpdateErr) done(replyAnswerUpdateErr);

								// Set assertions
								(replyAnswerUpdateRes.body._id).should.equal(replyAnswerSaveRes.body._id);
								(replyAnswerUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Reply answers if not signed in', function(done) {
		// Create new Reply answer model instance
		var replyAnswerObj = new ReplyAnswer(replyAnswer);

		// Save the Reply answer
		replyAnswerObj.save(function() {
			// Request Reply answers
			request(app).get('/reply-answers')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Reply answer if not signed in', function(done) {
		// Create new Reply answer model instance
		var replyAnswerObj = new ReplyAnswer(replyAnswer);

		// Save the Reply answer
		replyAnswerObj.save(function() {
			request(app).get('/reply-answers/' + replyAnswerObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', replyAnswer.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Reply answer instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Reply answer
				agent.post('/reply-answers')
					.send(replyAnswer)
					.expect(200)
					.end(function(replyAnswerSaveErr, replyAnswerSaveRes) {
						// Handle Reply answer save error
						if (replyAnswerSaveErr) done(replyAnswerSaveErr);

						// Delete existing Reply answer
						agent.delete('/reply-answers/' + replyAnswerSaveRes.body._id)
							.send(replyAnswer)
							.expect(200)
							.end(function(replyAnswerDeleteErr, replyAnswerDeleteRes) {
								// Handle Reply answer error error
								if (replyAnswerDeleteErr) done(replyAnswerDeleteErr);

								// Set assertions
								(replyAnswerDeleteRes.body._id).should.equal(replyAnswerSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Reply answer instance if not signed in', function(done) {
		// Set Reply answer user 
		replyAnswer.user = user;

		// Create new Reply answer model instance
		var replyAnswerObj = new ReplyAnswer(replyAnswer);

		// Save the Reply answer
		replyAnswerObj.save(function() {
			// Try deleting Reply answer
			request(app).delete('/reply-answers/' + replyAnswerObj._id)
			.expect(401)
			.end(function(replyAnswerDeleteErr, replyAnswerDeleteRes) {
				// Set message assertion
				(replyAnswerDeleteRes.body.message).should.match('User is not logged in');

				// Handle Reply answer error error
				done(replyAnswerDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		ReplyAnswer.remove().exec();
		done();
	});
});