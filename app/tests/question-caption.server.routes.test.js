'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	QuestionCaption = mongoose.model('QuestionCaption'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, questionCaption;

/**
 * Question caption routes tests
 */
describe('Question caption CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Question caption
		user.save(function() {
			questionCaption = {
				name: 'Question caption Name'
			};

			done();
		});
	});

	it('should be able to save Question caption instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Question caption
				agent.post('/question-captions')
					.send(questionCaption)
					.expect(200)
					.end(function(questionCaptionSaveErr, questionCaptionSaveRes) {
						// Handle Question caption save error
						if (questionCaptionSaveErr) done(questionCaptionSaveErr);

						// Get a list of Question captions
						agent.get('/question-captions')
							.end(function(questionCaptionsGetErr, questionCaptionsGetRes) {
								// Handle Question caption save error
								if (questionCaptionsGetErr) done(questionCaptionsGetErr);

								// Get Question captions list
								var questionCaptions = questionCaptionsGetRes.body;

								// Set assertions
								(questionCaptions[0].user._id).should.equal(userId);
								(questionCaptions[0].name).should.match('Question caption Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Question caption instance if not logged in', function(done) {
		agent.post('/question-captions')
			.send(questionCaption)
			.expect(401)
			.end(function(questionCaptionSaveErr, questionCaptionSaveRes) {
				// Call the assertion callback
				done(questionCaptionSaveErr);
			});
	});

	it('should not be able to save Question caption instance if no name is provided', function(done) {
		// Invalidate name field
		questionCaption.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Question caption
				agent.post('/question-captions')
					.send(questionCaption)
					.expect(400)
					.end(function(questionCaptionSaveErr, questionCaptionSaveRes) {
						// Set message assertion
						(questionCaptionSaveRes.body.message).should.match('Please fill Question caption name');
						
						// Handle Question caption save error
						done(questionCaptionSaveErr);
					});
			});
	});

	it('should be able to update Question caption instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Question caption
				agent.post('/question-captions')
					.send(questionCaption)
					.expect(200)
					.end(function(questionCaptionSaveErr, questionCaptionSaveRes) {
						// Handle Question caption save error
						if (questionCaptionSaveErr) done(questionCaptionSaveErr);

						// Update Question caption name
						questionCaption.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Question caption
						agent.put('/question-captions/' + questionCaptionSaveRes.body._id)
							.send(questionCaption)
							.expect(200)
							.end(function(questionCaptionUpdateErr, questionCaptionUpdateRes) {
								// Handle Question caption update error
								if (questionCaptionUpdateErr) done(questionCaptionUpdateErr);

								// Set assertions
								(questionCaptionUpdateRes.body._id).should.equal(questionCaptionSaveRes.body._id);
								(questionCaptionUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Question captions if not signed in', function(done) {
		// Create new Question caption model instance
		var questionCaptionObj = new QuestionCaption(questionCaption);

		// Save the Question caption
		questionCaptionObj.save(function() {
			// Request Question captions
			request(app).get('/question-captions')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Question caption if not signed in', function(done) {
		// Create new Question caption model instance
		var questionCaptionObj = new QuestionCaption(questionCaption);

		// Save the Question caption
		questionCaptionObj.save(function() {
			request(app).get('/question-captions/' + questionCaptionObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', questionCaption.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Question caption instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Question caption
				agent.post('/question-captions')
					.send(questionCaption)
					.expect(200)
					.end(function(questionCaptionSaveErr, questionCaptionSaveRes) {
						// Handle Question caption save error
						if (questionCaptionSaveErr) done(questionCaptionSaveErr);

						// Delete existing Question caption
						agent.delete('/question-captions/' + questionCaptionSaveRes.body._id)
							.send(questionCaption)
							.expect(200)
							.end(function(questionCaptionDeleteErr, questionCaptionDeleteRes) {
								// Handle Question caption error error
								if (questionCaptionDeleteErr) done(questionCaptionDeleteErr);

								// Set assertions
								(questionCaptionDeleteRes.body._id).should.equal(questionCaptionSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Question caption instance if not signed in', function(done) {
		// Set Question caption user 
		questionCaption.user = user;

		// Create new Question caption model instance
		var questionCaptionObj = new QuestionCaption(questionCaption);

		// Save the Question caption
		questionCaptionObj.save(function() {
			// Try deleting Question caption
			request(app).delete('/question-captions/' + questionCaptionObj._id)
			.expect(401)
			.end(function(questionCaptionDeleteErr, questionCaptionDeleteRes) {
				// Set message assertion
				(questionCaptionDeleteRes.body.message).should.match('User is not logged in');

				// Handle Question caption error error
				done(questionCaptionDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		QuestionCaption.remove().exec();
		done();
	});
});