exports.uploadFile = function(req, res) {
    // We are able to access req.files.file thanks to
    // the multiparty middleware
    // var path = require('path');
    // var file = req.files.file;
    // console.log(file.name);
    // console.log(file.type);
    // console.log(file.path);
    // return 'holaa';
    var fs = require('fs');
    setTimeout(function() {
        res.setHeader('Content-Type', 'text/html');
        console.log(req);
        if (req.files.length == 0 || req.files.file.size == 0) res.send({
            msg: 'No file uploaded at ' + new Date().toString()
        });
        else {
            var serverPath = './public/uploads/' + req.files.file.name;
            var file = req.files.file;
            fs.rename(file.path, serverPath, function(err) {
                if (err) throw err;
                else
                //res.end("Hello");
                    res.send({
                    msg: file.path
                });
            });
        }
    }, (req.param('delay', 'yes') == 'yes') ? 2000 : -1);
    //console.log(uploadPath);
}
exports.deleteFile = function(req, res) {
    //req.params.name
    var fs = require('fs');
    var serverPath = './public/uploads/' + req.params.name;
    //var filename = "D:\\temp\\temp.zip";
    fs.exists(serverPath, function(exists) {
        if (exists) {
            var tempFile = fs.openSync(serverPath, 'r');
            fs.closeSync(tempFile);
            console.log(tempFile);
            fs.unlink(serverPath, function(err) {
                if (err) {
                    throw err;
                    return res.status(400).send({
                        message: err
                    });
                    console.log(err)
                } else {
                    res.jsonp('ok');
                }
            });
            // console.log(serverPath);
        } else {
            res.status(400).send({
                message: 'no found file'
            });
        }
    });
}
