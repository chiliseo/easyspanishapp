'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	QuestionCaption = mongoose.model('QuestionCaption'),
	_ = require('lodash');

/**
 * Create a Question caption
 */
exports.create = function(req, res) {
	var questionCaption = new QuestionCaption(req.body);
	questionCaption.user = req.user;

	questionCaption.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(questionCaption);
		}
	});
};

/**
 * Show the current Question caption
 */
exports.read = function(req, res) {
	res.jsonp(req.questionCaption);
};

/**
 * Update a Question caption
 */
exports.update = function(req, res) {
	var questionCaption = req.questionCaption ;

	questionCaption = _.extend(questionCaption , req.body);

	questionCaption.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(questionCaption);
		}
	});
};

/**
 * Delete an Question caption
 */
exports.delete = function(req, res) {
	var questionCaption = req.questionCaption ;

	questionCaption.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(questionCaption);
		}
	});
};

/**
 * List of Question captions
 */
exports.list = function(req, res) { 
	QuestionCaption.find().sort('-created').populate('user', 'displayName').exec(function(err, questionCaptions) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(questionCaptions);
		}
	});
};

/**
 * Question caption middleware
 */
exports.questionCaptionByID = function(req, res, next, id) { 
	QuestionCaption.findById(id).populate('user', 'displayName').exec(function(err, questionCaption) {
		if (err) return next(err);
		if (! questionCaption) return next(new Error('Failed to load Question caption ' + id));
		req.questionCaption = questionCaption ;
		next();
	});
};

/**
 * Question caption authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.questionCaption.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
