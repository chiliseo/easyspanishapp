'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	ReplyAnswer = mongoose.model('ReplyAnswer'),
	_ = require('lodash');

/**
 * Create a Reply answer
 */
exports.create = function(req, res) {
	var replyAnswer = new ReplyAnswer(req.body);
	replyAnswer.user = req.user;

	replyAnswer.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(replyAnswer);
		}
	});


};


 exports.searchUserAndQuestion = function (req, res){
	// console.log("holaaaaaa"+req.user._id);
		var replyAnswer = new ReplyAnswer(req.body);
		// console.log(replyAnswer);
		var idUser = req.user._id;
		var idQuestion =replyAnswer.id_question;
		ReplyAnswer.find({$and:[{ user: idUser },{id_question: idQuestion}] })
		.sort('-created')
		.populate('user', 'displayName')
		.exec(function(err, replyAnswers) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				if (_.isEmpty(replyAnswers)) {
					console.log("hola");
					return true;
				}else {
					console.log("adiooos");
					return false;
				}
				// console.log(replyAnswers);
				// res.jsonp(replyAnswers);
			}
		});
		// next();
}
exports.createAndUpdate = function(req, res) {
// console.log("holaaaaaa"+req.user._id);
	// var replyAnswer = new ReplyAnswer(req.body);
	// // console.log(replyAnswer);
	// var idUser = req.user._id;
	// var idQuestion =replyAnswer.id_question;
	// console.log("holaaaaaa"+req.user._id);
		var replyAnswer = new ReplyAnswer(req.body);
		// console.log(replyAnswer);
		var idUser = req.user._id;
		var idQuestion =replyAnswer.id_question;
		ReplyAnswer.find({$and:[{ user: idUser },{id_question: idQuestion}] })
		.sort('-created')
		.populate('user', 'displayName')
		.exec(function(err, replyAnswers) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				// console.log(replyAnswers);
				if (_.isEmpty(replyAnswers)) {
					  ReplyAnswer = mongoose.model('ReplyAnswer');
						var replyAnswer = new ReplyAnswer(req.body);
						replyAnswer.user = req.user;
						replyAnswer.save(function(err) {
							if (err) {
								return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
							} else {
								// res.jsonp(replyAnswer);
							}
						});
				}else {
					var replyAnswerData = new ReplyAnswer(req.body);
					var replyPorcentOri = replyAnswers[0].replyPorcent;
					var replyPorcentReq = replyAnswerData.replyPorcent;
					// console.log(replyPorcentOri);
					// console.log(replyPorcentReq);
					// console.log(replyAnswerData);

					ReplyAnswer.findByIdAndUpdate(replyAnswers[0]._id, { replyPorcent: replyPorcentReq }, function(err, replyAnswers) {
					  if (err) throw err;

					  // we have the updated user returned to us
						// console.log(replyAnswers.replyPorcent);
					  // console.log(replyAnswers);
					});
					// console.log(replyAnswers[0]._id);
					// ReplyAnswer.findById(replyAnswers[0]._id)
					// .populate('user', 'displayName')
					// .exec(function(err, replyAnswer) {
					// 	if (err) return next(err);
					// 	if (!replyAnswer) return next(new Error('Failed to load Reply answer ' + id));
					// 	idAnswer = replyAnswer ;
					// 	// next();
					// 	console.log(idAnswer);
					// });


				}
				// console.log(replyAnswers);
				// res.jsonp(replyAnswers);
			}
		});
};
/**
 * Show the current Reply answer
 */
exports.read = function(req, res) {
	res.jsonp(req.replyAnswer);
};

/**
 * Update a Reply answer
 */
exports.update = function(req, res) {
	var replyAnswer = req.replyAnswer ;

	replyAnswer = _.extend(replyAnswer , req.body);

	replyAnswer.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(replyAnswer);
		}
	});
};

/**
 * Delete an Reply answer
 */
exports.delete = function(req, res) {
	var replyAnswer = req.replyAnswer ;

	replyAnswer.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(replyAnswer);
		}
	});
};

/**
 * List of Reply answers
 */
exports.list = function(req, res) {
	ReplyAnswer.find().sort('-created').populate('user', 'displayName').exec(function(err, replyAnswers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(replyAnswers);
		}
	});
};

/**
 * List of Reply answers by Users
 */
exports.answersByUser = function(req, res) {
	// var req.user;
	ReplyAnswer.find({user:req.user}).sort('-created').populate('user', 'displayName').exec(function(err, replyAnswers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(replyAnswers);
		}
	});
};

/**
 * Reply answer middleware
 */
exports.replyAnswerByID = function(req, res, next, id) {
	ReplyAnswer.findById(id).populate('user', 'displayName').exec(function(err, replyAnswer) {
		if (err) return next(err);
		if (! replyAnswer) return next(new Error('Failed to load Reply answer ' + id));
		req.replyAnswer = replyAnswer ;
		next();
	});
};

/**
 * Reply answer authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.replyAnswer.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
