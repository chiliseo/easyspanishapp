'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Speech = mongoose.model('Speech'),
	_ = require('lodash');

/**
 * Create a Speech
 */
exports.create = function(req, res) {
	var speech = new Speech(req.body);
	speech.user = req.user;

	speech.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(speech);
		}
	});
};

/**
 * Show the current Speech
 */
exports.read = function(req, res) {
	res.jsonp(req.speech);
};

/**
 * Update a Speech
 */
exports.update = function(req, res) {
	var speech = req.speech ;

	speech = _.extend(speech , req.body);

	speech.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(speech);
		}
	});
};

/**
 * Delete an Speech
 */
exports.delete = function(req, res) {
	var speech = req.speech ;

	speech.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(speech);
		}
	});
};

/**
 * List of Speeches
 */
exports.list = function(req, res) {
	Speech.find().sort('-created').populate('user', 'displayName').exec(function(err, speeches) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(speeches);
		}
	});
};

/**
 * Speech middleware
 */
exports.speechByID = function(req, res, next, id) {
	Speech.findById(id).populate('user', 'displayName').exec(function(err, speech) {
		if (err) return next(err);
		if (! speech) return next(new Error('Failed to load Speech ' + id));
		req.speech = speech ;
		next();
	});
};

exports.videoByID = function(req, res) {
	// Speech.find().where('id_video')
	// .equals(id)
	// .populate('user', 'displayName')
	// .exec(function(err, speech) {
	// 	if (err) {
	// 	 return res.status(400).send({
	// 		message: errorHandler.getErrorMessage(err)
	// 	 });
	// 	} else {
	// 	 res.jsonp(results);
	// 	}
	// });
	// var
	var QuestionCaption = mongoose.model('QuestionCaption');
	QuestionCaption.find({id_video: req.query.i})
	.populate('user', 'displayName')
	.populate('id_video')
	.exec(function(err, speech) {
		if (err) {
		 return res.status(400).send({
			message: errorHandler.getErrorMessage(err)
		 });
		} else {
		 res.jsonp(speech);
		}
	});
	// QuestionCaption
};

/**
 * Speech authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.speech.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
