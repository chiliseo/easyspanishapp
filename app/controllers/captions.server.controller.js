'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Caption = mongoose.model('Caption'),
	_ = require('lodash');

/**
 * Create a Caption
 */
exports.create = function(req, res) {
	var caption = new Caption(req.body);
	caption.user = req.user;

	caption.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(caption);
		}
	});
};

/**
 * Show the current Caption
 */
exports.read = function(req, res) {
	res.jsonp(req.caption);
};

/**
 * Update a Caption
 */
exports.update = function(req, res) {
	var caption = req.caption ;

	caption = _.extend(caption , req.body);

	caption.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(caption);
		}
	});
};

/**
 * Delete an Caption
 */
exports.delete = function(req, res) {
	var caption = req.caption ;

	caption.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(caption);
		}
	});
};

/**
 * List of Captions
 */
exports.list = function(req, res) { 
	Caption.find().sort('-created').populate('user', 'displayName').exec(function(err, captions) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(captions);
		}
	});
};

/**
 * Caption middleware
 */
exports.captionByID = function(req, res, next, id) { 
	Caption.findById(id).populate('user', 'displayName').exec(function(err, caption) {
		if (err) return next(err);
		if (! caption) return next(new Error('Failed to load Caption ' + id));
		req.caption = caption ;
		next();
	});
};

/**
 * Caption authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.caption.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
