'use strict';
module.exports = function(app) {
    var express = require('express');
    var bodyParser = require('body-parser');
    var multer = require('multer');
    var methodOverride = require('method-override');
    app.use(multer({dest:'./public/uploads/'}));
    // app.use(multer());
    // parse application/json
    app.use(bodyParser.json());
    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({
        extended: true
    }));
   app.use(methodOverride());
   app.use(express.static(__dirname));
        // Requires controller
        //../../app/controllers/upload.server.controller
    var uploadController = require('../../app/controllers/upload.server.controller');
    // Example endpoint
    app.route('/uploads').post(uploadController.uploadFile);
    app.route('/deletefile/:name').get(uploadController.deleteFile);
};
