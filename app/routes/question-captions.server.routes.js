'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var questionCaptions = require('../../app/controllers/question-captions.server.controller');

	// Question captions Routes
	app.route('/question-captions')
		.get(questionCaptions.list)
		.post(users.requiresLogin, questionCaptions.create);

	app.route('/question-captions/:questionCaptionId')
		.get(questionCaptions.read)
		.put(users.requiresLogin, questionCaptions.hasAuthorization, questionCaptions.update)
		.delete(users.requiresLogin, questionCaptions.hasAuthorization, questionCaptions.delete);

	// Finish by binding the Question caption middleware
	app.param('questionCaptionId', questionCaptions.questionCaptionByID);
};
