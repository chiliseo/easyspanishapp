'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var captions = require('../../app/controllers/captions.server.controller');

	// Captions Routes
	app.route('/captions')
		.get(captions.list)
		.post(users.requiresLogin, captions.create);

	app.route('/captions/:captionId')
		.get(captions.read)
		.put(users.requiresLogin, captions.hasAuthorization, captions.update)
		.delete(users.requiresLogin, captions.hasAuthorization, captions.delete);

	// Finish by binding the Caption middleware
	app.param('captionId', captions.captionByID);
};
