'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var replyAnswers = require('../../app/controllers/reply-answers.server.controller');

	// Reply answers Routes
	app.route('/reply-answers')
		.get(replyAnswers.list)
		.post(users.requiresLogin, replyAnswers.createAndUpdate);

	app.route('/reply-answers/:replyAnswerId')
		.get(replyAnswers.read)
		.put(users.requiresLogin, replyAnswers.hasAuthorization, replyAnswers.update)
		.delete(users.requiresLogin, replyAnswers.hasAuthorization, replyAnswers.delete);

	app.route('/reply-answersByUser')
		.get(replyAnswers.answersByUser);
	// Finish by binding the Reply answer middleware
	app.param('replyAnswerId', replyAnswers.replyAnswerByID);
};
