'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var speeches = require('../../app/controllers/speeches.server.controller');

	// Speeches Routes
	app.route('/speeches')
		.get(speeches.list)
		.post(users.requiresLogin, speeches.create);

	app.route('/speeches/:speechId')
		.get(speeches.read)
		.put(users.requiresLogin, speeches.hasAuthorization, speeches.update)
		.delete(users.requiresLogin, speeches.hasAuthorization, speeches.delete);

		app.route('/asnwerByVideoId')
	 		.get(speeches.videoByID);

	// Finish by binding the Speech middleware
	app.param('speechId', speeches.speechByID);
	// app.param('videoId', speeches.videoByID);

};
