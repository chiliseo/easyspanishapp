'use strict';

module.exports = function(app) {
	// Root routing
	var watson = require('watson-developer-cloud'),
		extend = require('util')._extend,
		vcapServices = require('vcap_services');
	var expressBrowserify = require('express-browserify');
	app.all('*', function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Headers", "X-Requested-With");
	  next();
	 });
	var core = require('../../app/controllers/core.server.controller');
	app.route('/').get(core.index);
	app.all('*', function(req, res, next) {
	       res.header("Access-Control-Allow-Origin", "*");
	       res.header("Access-Control-Allow-Headers", "X-Requested-With");
	       res.header('Access-Control-Allow-Headers', 'Content-Type');
	       res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type")
	       next();
	});


	// automatically compile and serve the front-end js
	app.get('/js/index.js', expressBrowserify('./src/index.js', {
	  watch: process.env.NODE_ENV !== 'production'
	}));
	// For local development, replace username and password
	var textToSpeech = watson.text_to_speech({
	  version: 'v1',
	  username: 'b914e12d-9d10-4f5d-9764-7894f7dd0acb',
	  password: '6rbBqYOd3ds8'
	});
	app.get('/api/synthesize', function(req, res, next) {
	  var transcript = textToSpeech.synthesize(req.query);
	  transcript.on('response', function(response) {
	    if (req.query.download) {
	      response.headers['content-disposition'] = 'attachment; filename=transcript.ogg';
	    }
	  });
	  transcript.on('error', function(error) {
	    next(error);
	  });
	  transcript.pipe(res);
	});
	// For local development, replace username and password
	var config = extend({
	  version: 'v1',
	  url: 'https://stream.watsonplatform.net/speech-to-text/api',
	  username: '17fc4a4a-06c6-4373-8031-ebe1b2508133',
	  password: '8g2jSkIqtEpv'
	}, vcapServices.getCredentials('speech_to_text'));

	var authService = watson.authorization(config);

	app.post('/api/token', function(req, res, next) {
		res.header("Access-Control-Allow-Origin", req.headers.origin);
	  authService.getToken({url: config.url}, function(err, token) {
	    if (err)
	      next(err);
	    else
	      res.send(token);
	  });
	});

};
