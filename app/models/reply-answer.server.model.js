'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Reply answer Schema
 */
var ReplyAnswerSchema = new Schema({
	id_question:{
		type:Schema.ObjectId,
		ref:'QuestionCaption'
	},
	replyPorcent:{
		type:String
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('ReplyAnswer', ReplyAnswerSchema);
