'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Question caption Schema
 */
var QuestionCaptionSchema = new Schema({
	id_video:{
		type: Schema.ObjectId,
		ref: 'Video'
	},
	question_type:{
		type: String,
		default: '',
		required: 'Please fill Question type ',
		trim: true
	},
	start_second:{
		type: Number,
		default: '',
		required: 'Please fill start second',
		trim: true
	},
	end_second:{
		type: Number,
		default: '',
		required: 'Please fill end second',
		trim: true
	},
	start_text: {
		type: String,
		default: '',
		required: 'Please fill start text',
		trim: true
	},
	answer_text: {
		type: String,
		default: '',
		required: 'Please fill answer text',
		trim: true
	},
	end_text: {
		type: String,
		default: '',
		required: 'Please fill end text',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('QuestionCaption', QuestionCaptionSchema);
