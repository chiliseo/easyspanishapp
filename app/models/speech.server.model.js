'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
/**
 * Speech Schema
 */
var SpeechSchema = new Schema({
	id_video:{
		type: Schema.ObjectId,
		ref: 'Video'
	},
	name: {
		type: String,
		default: '',
		required: 'Please fill Speech name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Speech', SpeechSchema);
