'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Caption Schema
 */
var CaptionSchema = new Schema({
	id_video: {
		type: Schema.ObjectId,
		ref: 'Video'
	},
	url: {
		type: String,
		default: '',
		required: 'please fill Video url',
		trim: true
	},
	name: {
		type: String,
		default: '',
		required: 'Please fill Caption name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Caption', CaptionSchema);
