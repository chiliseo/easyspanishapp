'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Video Schema
 */
var VideoSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Video name',
		trim: true
	},
	url: {
		type: String,
		default: '',
		required: 'please fill Video url',
		trim: true
	},
	language: {
		type: String,
		default: '',
		required: 'please fill Video language',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Video', VideoSchema);
