'use strict';

module.exports = {
	app: {
		title: 'easySpanish',
		description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
		keywords: 'MongoDB, Express, AngularJS, Node.js'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				 'public/lib/bootstrap/dist/css/bootstrap.css',
				 'public/lib/bootstrap/dist/css/bootstrap-theme.css',
				'public/lib/assets/plugins/pace/pace-theme-flash.css',
				'public/lib/assets/plugins/boostrapv3/css/bootstrap.min.css',
				'public/lib/assets/plugins/font-awesome/css/font-awesome.css',
				'public/lib/assets/plugins/jquery-scrollbar/jquery.scrollbar.css',
				'public/lib/assets/plugins/bootstrap-select2/select2.css',
				'public/lib/assets/plugins/switchery/css/switchery.min.css',
				'public/lib/pages/css/pages-icons.css',
				'public/lib/pages/css/pages.css',
				'public/lib/easy/style.css',
			],
			js: [
				'public/lib/js/jquery.min.js',
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-cookies/angular-cookies.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-touch/angular-touch.js',
				'public/lib/angular-sanitize/angular-sanitize.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/angular-multi-step-form/dist/browser/angular-multi-step-form.js',
				'public/lib/angular-file-model/angular-file-model.js'
				// // 'public/lib/js/jwplayer/jwplayer.js',
				// 'public/lib/assets/plugins/pace/pace.min.js',
				// 'public/lib/assets/plugins/jquery/jquery-1.11.1.min.js',
				// 'public/lib/assets/plugins/modernizr.custom.js',
				// 'public/lib/assets/plugins/jquery-ui/jquery-ui.min.js',
				// 'public/lib/assets/plugins/boostrapv3/js/bootstrap.min.js',
				// 'public/lib/assets/plugins/jquery/jquery-easy.js',
				// 'public/lib/assets/plugins/jquery-unveil/jquery.unveil.min.js',
				// 'public/lib/assets/plugins/jquery-bez/jquery.bez.min.js',
				// 'public/lib/assets/plugins/jquery-ios-list/jquery.ioslist.min.js',
				// 'public/lib/assets/plugins/jquery-actual/jquery.actual.min.js',
				// 'public/lib/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
				// 'public/lib/assets/plugins/bootstrap-select2/select2.min.js',
				// 'public/lib/assets/plugins/classie/classie.js',
				// 'public/lib/assets/plugins/switchery/js/switchery.min.js',
				// // 'public/lib/js/main.js',
				// 'public/lib/pages/js/pages.min.js',
				// 'public/lib/assets/js/scripts.js'

			]
		},
		css: [
			 'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
